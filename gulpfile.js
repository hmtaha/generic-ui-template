const gulp = require('gulp');
const log = require('fancy-log');
const debug = require('gulp-debug');

const concat = require('gulp-concat');
// const filter = require('gulp-filter');
const sass = require('gulp-sass');
const angularSortByContent = require('gulp-angular-filesort');

const inject = require('gulp-inject');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const flatten = require('gulp-flatten');

const rev = require('gulp-rev');

// const sourcemaps = require('gulp-sourcemaps');
// const browserify = require('browserify');
const gulpBrowserify = require('gulp-browserify');
// const babelify = require('babelify');
const babel = require('gulp-babel');

const gulpSequence = require('gulp-sequence');
// const cacheBuster = require('gulp-cachebust');
const eventStream = require('event-stream');
const gulpOrder = require('gulp-order');
const _del = require('gulp-clean');

const browserSync = require('browser-sync').create();


const scripts = require('./scripts');
const styles = require('./styles');
const assets = require('./assets');
const locales = require('./locales');


const eslint = require('gulp-eslint');
let eslintConf = require('./eslint-configuration');

let mode = {isDevMode: false};

gulp.task('eslint', function () {
    return gulp.src(scripts.app)
        .pipe(eslint(eslintConf))
        // Format all results at once, at the end
        .pipe(eslint.format())
        // failAfterError will emit an error (fail) just before the stream finishes if any file has an error
        .pipe(eslint.failAfterError());
});

gulp.task('js', function () {
    let vendorSources = gulp.src(scripts.vendor);
    let appSources = gulp.src(scripts.app).// .pipe(sourcemaps.init())
        //@babel/preset-env
        pipe(babel({
            "presets": ["@babel/preset-env"]
        })).on('error', function (err) {
            log.error(err);
        })
    ;

    let source;
    if (mode.isDevMode) {
        source = eventStream.merge(
            vendorSources.pipe(gulp.dest('./dist/js/vendor')),
            appSources.pipe(gulp.dest('./dist/js/app'))
        );
    } else {
        source = eventStream.merge(
            vendorSources
                .pipe(gulpOrder(scripts.vendor.map(path => path.substring(2)), {base: './'}))
                .pipe(debug())
                .pipe(concat('main-vendor.js'))
                .pipe(uglify())
                .on('error', function (err) {
                    log.error(err);
                })
                .pipe(rev())
                .pipe(gulp.dest('./dist/js/vendor')),

            appSources
                .pipe(angularSortByContent())
                .pipe(concat('main-app.js'))
                .pipe(uglify())
                .on('error', function (err) {
                    throw err;
                })
                .pipe(gulpBrowserify({
                    insertGlobals: true,
                    debug: true
                }))
                // .pipe(sourcemaps.write())
                .pipe(rev())
                .pipe(gulp.dest('./dist/js/app'))
        );
    }
    return source
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('fonts', function () {
    gulp.src(styles.fonts)
        .pipe(flatten())
        .pipe(gulp.dest("dist/styles/fonts"));
    gulp.src(styles.webFonts)
        .pipe(flatten())
        .pipe(gulp.dest("dist/styles/webfonts"));

});

gulp.task('styles', ['fonts'], function () {
    let vendorSources = gulp.src(styles.vendor);
    let appSources = gulp.src(styles.app);

    let source;

    if (mode.isDevMode) {
        source = eventStream.merge(
            vendorSources
                .pipe(sass().on('error', sass.logError))
                .pipe(gulp.dest('dist/styles/vendor')),
            appSources
                .pipe(sass().on('error', sass.logError))
                .pipe(gulp.dest('dist/styles/app'))
        );

    } else {
        source = eventStream.merge(
            vendorSources
                .pipe(sass().on('error', sass.logError))
                .pipe(concat('main-vendor.css'))
                .pipe(cleanCSS())
                .pipe(rev())
                .pipe(gulp.dest('dist/styles/vendor')),
            appSources
                .pipe(sass().on('error', sass.logError))
                .pipe(concat('main-app.css'))
                .pipe(cleanCSS())
                .pipe(rev())
                .pipe(gulp.dest('dist/styles/app'))
        );
    }
    return source.pipe(browserSync.reload({stream: true}));
});


gulp.task('html', function () {
    let orderArray = scripts.vendor.map(path => {
        let split = path.split('/');
        return "dist/js/vendor/" + split[split.length - 1];
    });
    let jsAppSources = gulp.src(['./dist/js/app/**/**/*.js']);
    let jsVendorSources = gulp.src(['./dist/js/vendor/**/**/*.js']);
    let cssAppSources = gulp.src(['./dist/styles/app/**/**/*.css']);
    let cssVendorSources = gulp.src(['./dist/styles/vendor/**/**/*.css']);

    let appInjectionOptions = {
        removeTags: true,
        ignorePath: ['dist'],
        addRootSlash: false,
        starttag: '<!-- inject:app:{{ext}} -->'
    };
    let vendorInjectionOptions = {
        removeTags: true,
        ignorePath: ['dist'],
        addRootSlash: false,
        starttag: '<!-- inject:vendor:{{ext}} -->'
    };

    return gulp.src('./src/**/*.html')
        .pipe(inject(jsVendorSources.pipe(gulpOrder(orderArray, {base: './'})), vendorInjectionOptions))
        .pipe(inject(jsAppSources.pipe(angularSortByContent()).pipe(debug()), appInjectionOptions))

        .pipe(inject(cssVendorSources, vendorInjectionOptions))
        .pipe(inject(cssAppSources, appInjectionOptions))

        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('clean', function () {
    return gulp.src(["./dist"], {read: false})
        .pipe(_del().on('error', err => {
            throw err;
        }));
});

gulp.task('assets', function () {
    gulp.src(assets.images)
        .pipe(gulp.dest('./dist/assets/images'));
    gulp.src('./src/favicon.png')
        .pipe(gulp.dest('./dist/'));
});

gulp.task('i18n', function () {
    gulp.src(locales.files)
        .pipe(gulp.dest('./dist/'));
});

gulp.task('build', function (done) {
    gulpSequence('clean', 'eslint', ['styles', 'js', 'assets'], 'html', 'i18n', done);
});

gulp.task('browser-sync', function () {
    browserSync.init(null, {
        open: false,
        ghostMode: {
            clicks: false,
            forms: false,
            scroll: false
        },
        codeSync: true,
        server: {
            baseDir: 'dist'
        }
    });
});

gulp.task('serve', function (done) {
    mode.isDevMode = true;
    setESLintOnWarningMode();
    gulpSequence('build', 'browser-sync', done);
    gulp.watch(['./src/**/*.scss'], ['styles']);
    gulp.watch(['./src/**/*.js'], ['js']);
    gulp.watch(['./src/**/*.html'], ['html']);
    gulp.watch(['./src/assets/**/*'], ['assets']);

});

gulp.task('prod', function (done) {
    mode.isDevMode = false;
    gulpSequence('build', 'browser-sync', done);

    gulp.watch(['./src/**/*'], ['build']);
});

function setESLintOnWarningMode() {
    Object.keys(eslintConf.rules)
        .forEach((k) => {
            let ruleValue = eslintConf.rules[k];
            if (Array.isArray(ruleValue)) {
                eslintConf.rules[k][0] = "warn";
            } else {
                eslintConf.rules[k] = "warn";
            }
        });
}
