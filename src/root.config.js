(function () {
    angular.module('app')
        .config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function Config($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when('', '/root');
        $urlRouterProvider.when('/', '/root');
        $urlRouterProvider.otherwise('/404');
        $stateProvider
            .state('root', {
                url: '/root',
                component: 'root'
            })
            .state('404', {
                url: '/404',
                template: '<h1 class="error">Error 404</h1>',
                controller: function () {
                }
            })
        ;
    }
})();
